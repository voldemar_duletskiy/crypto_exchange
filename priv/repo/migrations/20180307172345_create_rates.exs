defmodule CryptoExchange.Repo.Migrations.CreateRates do
  use Ecto.Migration

  def change do
    create table(:rates) do
      add :name, :string, null: false
      add :rate, :float, null: false

      timestamps(updated_at: false)
    end

    create index(:rates, [:name, :inserted_at])
  end
end
