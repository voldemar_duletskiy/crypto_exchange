defmodule CryptoExchange.Rate do
  use Ecto.Schema
  import Ecto.{Changeset, Query}

  @attributes [:name, :rate, :inserted_at]
  @required_attributes [:name, :rate]
  @currencies Application.get_env(:crypto_exchange, :currencies)

  schema "rates" do
    field(:name, :string)
    field(:rate, :float)

    timestamps(updated_at: false)
  end

  def changeset(rate, params \\ %{}) do
    rate
    |> cast(params, @attributes)
    |> validate_required(@required_attributes)
    |> validate_inclusion(:name, @currencies)
  end

  def convert_to_usd(rate), do: 1.0 / rate

  def to_hash(%{name: name, rate: rate}) do
    %{rate_usd: convert_to_usd(rate), name: name}
  end

  def find_by(name) do
    from r in __MODULE__,
         where: r.name == ^name,
         order_by: [desc: r.inserted_at],
         limit: 1
  end

  def find_by(name, nil), do: find_by(name)

  def find_by(name, date) do
    from r in find_by(name),
         where: r.inserted_at <= ^date
  end
end
