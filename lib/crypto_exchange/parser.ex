defmodule CryptoExchange.Parser do
  @base "USD"
  @url "https://min-api.cryptocompare.com/data/pricemulti?fsyms=#{@base}&tsyms=ETH,BTC,BCH"

  def parse() do
    %HTTPotion.Response{body: body} = HTTPotion.get!(@url)

    body
    |> Poison.decode!()
    |> Map.get(@base)
  end
end
