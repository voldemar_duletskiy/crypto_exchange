defmodule CryptoExchange.Context.Rates do
  alias CryptoExchange.{Repo, Rate}

  @parser Application.get_env(:crypto_exchange, :parser)
  @currencies Application.get_env(:crypto_exchange, :currencies)

  def fetch_rates() do
    @parser.parse
    |> Map.to_list()
    |> Enum.map(fn {name, rate} ->
      {:ok, item} = create(%{name: name, rate: rate})
      Rate.to_hash(item)
    end)
  end

  def list(unix_time)
      when is_integer(unix_time),
      do: unix_time |> Timex.from_unix() |> list()

  def list(time) do
    iterate_with_callback(&Rate.find_by(&1, time))
  end

  def list() do
    iterate_with_callback(&Rate.find_by(&1))
  end

  defp iterate_with_callback(callback) do
    @currencies
    |> List.foldl([], fn rate, acc ->
      rate
      |> callback.()
      |> Repo.one()
      |> case do
           nil -> acc
           rate -> acc ++ [Rate.to_hash(rate)]
         end
    end)
  end

  def create(params) do
    %Rate{}
    |> Rate.changeset(params)
    |> Repo.insert()
  end

  def calculate(name, amount), do: calculate(name, amount, nil)

  def calculate(name, amount, date) when is_integer(date) do
    calculate(name, amount, DateTime.from_unix!(date))
  end

  def calculate(name, amount, date) do
    Rate.find_by(name, date)
    |> Repo.one()
    |> case do
      nil -> {:error, "no rates for such currency"}
      rate -> amount / rate.rate
    end
  end
end
