defmodule CryptoExchange.Application do
  use Application

  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    import Supervisor.Spec

    # Define workers and child supervisors to be supervised
    children = [
      supervisor(CryptoExchange.Repo, []),
      supervisor(CryptoExchangeWeb.Endpoint, []),
      supervisor(Phoenix.PubSub.PG2, [:pubsub_rates,  []]),
      worker(CryptoExchangeWeb.Worker.RateGrabber, []),
    ]

    opts = [strategy: :one_for_one, name: CryptoExchange.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    CryptoExchangeWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
