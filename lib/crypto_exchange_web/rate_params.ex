defmodule CryptoExchangeWeb.RateParams do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key false

  schema "RateParams" do
    field :name, :string
    field :amount, :float
    field :date, :integer, default: nil
  end

  def changeset(params \\ :empty) do
    %__MODULE__{}
    |> cast(params, ~w(name amount date))
    |> validate_required([:name, :amount])
    |> validate_number(:amount, greater_than_or_equal_to: 0)
  end

  def cast(changeset), do: apply_changes(changeset)
end
