require Logger

defmodule CryptoExchangeWeb.Worker.RateGrabber do
  @moduledoc false
  use GenServer

  alias CryptoExchange.{Context.Rates}
  alias CryptoExchangeWeb.{RatesChannel, Endpoint}

  @interval 10  * 1000

  def start_link() do
    state = Enum.into(%{interval: @interval},  [])
    GenServer.start_link(__MODULE__, state, name: __MODULE__)
  end

  def init(state) do
    work(state)
    {:ok, state}
  end

  def handle_info(:run, state) do
    work(state)
    {:noreply, state}
  end

  defp work(state) do
    rates = Rates.fetch_rates()

    Logger.info "Crypto currences has been parsed"

    Endpoint.broadcast!(RatesChannel.topic(), "new", %{rates: rates})

    Process.send_after(self(), :run, state[:interval])
  end
end
