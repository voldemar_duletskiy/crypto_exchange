defmodule CryptoExchangeWeb.RateController do
  use CryptoExchangeWeb, :controller

  alias CryptoExchange.Context.Rates
  alias CryptoExchangeWeb.RateParams

  plug(:validate_params)

  def show(%{assigns: %{rate_params: %{name: name, amount: amount, date: date}}} = conn, _) do
    Rates.calculate(name, amount, date)
    |> response(conn)
  end

  defp response({:error, error}, conn) do
    conn
    |> put_status(422)
    |> json(%{error: error})
  end

  defp response(amount, conn) do
    conn
    |> put_status(200)
    |> json(%{name: "USD", amount: format_amount(amount)})
  end

  defp validate_params(%{params: params} = conn, _params) do
    params
    |> RateParams.changeset()
    |> process_request(conn)
  end

  defp process_request(%{valid?: true} = changeset, conn) do
    assign(conn, :rate_params, RateParams.cast(changeset))
  end

  defp process_request(_, conn) do
    conn
    |> put_status(400)
    |> json(%{error: "Error, wrong parameters supplied!"})
    |> halt
  end

  defp format_amount(amount) do
    :erlang.float_to_binary(amount, decimals: 16)
  end
end
