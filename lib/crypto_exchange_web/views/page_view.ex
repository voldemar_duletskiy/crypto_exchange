defmodule CryptoExchangeWeb.PageView do
  use CryptoExchangeWeb, :view

  def currencies(), do: Application.get_env(:crypto_exchange, :currencies)
end
