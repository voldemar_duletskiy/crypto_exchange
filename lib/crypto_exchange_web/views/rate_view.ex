defmodule CryptoExchangeWeb.RateView do
  use CryptoExchangeWeb, :view

  @attributes [:currency, :rate]

  def render("show.json", rate) do
    rate |> Map.take(@attributes)
  end
end
