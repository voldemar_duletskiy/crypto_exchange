import {Socket} from "phoenix"
import {datepicker} from "./datepicker.min"

function addRates(rates) {
	var rates_element = $('#rates')
	var html = ""

	$("#stub").remove()
	rates_element.empty()

	rates.forEach(function(item, i, arr) {
		html += "<li class='list-group-item text-left'><b>" + item.name + "</b>: " + item.rate_usd + "</li>"
  });

  rates_element.html(html)
}

function to_unix(date) { return Math.floor(date.getTime() / 1000) }

let socket = new Socket("/socket", {params: {token: window.userToken}})

socket.connect()

let channel = socket.channel("rates:get", {})
channel.join()

channel.on("update", msg => addRates(msg.rates))

$('#timestamp').datepicker({
	timepicker: true,
	timeFormat: "hh:ii:00",
	dateFormat: "yyyy-mm-dd"
});

$("#change_rate").on("click", (event) => {
	try {
		var inserted_at = $('#timestamp').datepicker().data('datepicker').lastSelectedDate
		var payload = to_unix(inserted_at)

		channel.push("change_timestamp", payload)
	} catch(err) { console.log(err) }
});

export default socket
