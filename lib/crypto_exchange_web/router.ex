defmodule CryptoExchangeWeb.Router do
  use CryptoExchangeWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", CryptoExchangeWeb do
    pipe_through(:api)

    resources "/rates/calc", RateController, only: [:show], param: "name"
  end

  get "/", CryptoExchangeWeb.PageController, :index
  get "/edit", CryptoExchangeWeb.PageController, :edit
end
