require Logger

defmodule CryptoExchangeWeb.RatesChannel do
  use CryptoExchangeWeb, :channel
  alias CryptoExchangeWeb.Endpoint
  alias CryptoExchange.{Context.Rates}
  alias Phoenix.Socket.Broadcast

  @topic "rates_updated"
  def topic, do: @topic

  def join("rates:get", _message, socket) do
    Logger.info("New client connected")
    Endpoint.subscribe(@topic)

    socket = assign(socket, :timestamp, nil)

    send(self(), :after_join)
    {:ok, socket}
  end

  def handle_in("change_timestamp", timestamp, socket) do
    Logger.info("Changing timestamp for all clients")

    # Broadcasting to timestamp handler
    Endpoint.broadcast!(@topic, "update_timestamp", %{timestamp: timestamp})
    # Broadcasting to clients
    broadcast(socket, "update", update_ts_payload(timestamp))

    {:noreply, socket}
  end

  def handle_info(:after_join, socket), do: push_update(socket, Rates.list())

  def handle_info(%Broadcast{topic: @topic, event: event, payload: payload}, socket) do
    handle_broadcast(event, payload, socket)
  end

  defp handle_broadcast("update_timestamp", %{timestamp: timestamp}, socket) do
    Logger.info("Broadcast: update_timestamp")

    socket = assign(socket, :timestamp, timestamp)
    {:noreply, socket}
  end

  defp handle_broadcast("new", %{rates: rates}, %{assigns: %{timestamp: nil}} = socket) do
    Logger.info("Broadcast: new")

    push_update(socket, rates)
  end

  defp handle_broadcast("new", _, socket), do: {:noreply, socket}

  defp update_ts_payload(timestamp) do
    %{rates: Rates.list(timestamp)}
  end

  defp push_update(socket, rates) do
    push(socket, "update", %{rates: rates})

    {:noreply, socket}
  end
end
