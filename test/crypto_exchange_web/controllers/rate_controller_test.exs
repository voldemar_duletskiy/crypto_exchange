defmodule CryptoExchange.RateControllerTest do
  use CryptoExchange.ConnCase

  alias CryptoExchange.Context.Rates

  describe "show/1" do
    @unix_utc DateTime.to_unix(Timex.now())

    test "get BTC rate" do
      Rates.fetch_rates()

      build_conn()
      |> get("/api/rates/calc/BTC", %{amount: 2.0})
      |> json_response(200)
    end

    test "get BTC rate with date" do
      Rates.fetch_rates()

      build_conn()
      |> get("/api/rates/calc/BTC", %{amount: 2.0, date: @unix_utc})
      |> json_response(200)
    end

    test "bad arguments" do
      build_conn()
      |> get("/api/rates/calc/BTC")
      |> json_response(400)
    end
  end
end