defmodule CryptoExchangeWeb.RateParamsTest do
  use CryptoExchange.ConnCase, async: true

  alias CryptoExchangeWeb.RateParams

  describe "changeset/1" do
    setup do
      %{name: "BTC", amount: "123", date:  DateTime.to_unix(Timex.now)}
    end

    test "required fields", params do
      assert RateParams.changeset(params).valid?
    end

    test "without required field", params do
      refute RateParams.changeset(Map.delete(params, :name)).valid?
    end
  end
end
