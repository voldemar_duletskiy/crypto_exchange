defmodule CryptoExchangeWeb.RatesChannelTest do
  use CryptoExchangeWeb.ChannelCase

  alias CryptoExchangeWeb.{RatesChannel, Endpoint}

  setup do
    {:ok, _, socket} =
      socket("user_id", %{})
      |> subscribe_and_join(RatesChannel, "rates:get")

    {:ok, socket: socket}
  end

  @payload %{
    rates: [
      %{name: "BTC", rate_usd: 9813.542688910697},
      %{name: "BCH", rate_usd: 1080.8473843493298},
      %{name: "ETH", rate_usd: 744.047619047619}
    ]
  }

  def broadcast! do
    Endpoint.broadcast!(RatesChannel.topic, "new", @payload)
  end

  test "testing state after channel join", %{socket: socket} do
    assert %{assigns: %{timestamp: nil}} = socket

    assert_push("update", @payload)
  end

  test "testing change timestamp", %{socket: socket} do
    time = Timex.now |> Timex.to_unix

    push socket, "change_timestamp", time - 1_000_000

    assert_push "update", %{rates: []}

    push socket, "change_timestamp", time

    assert_push "update", @payload
  end

  test "broadcast to channel" do
    assert_push "update", @payload

    broadcast!()

    assert_push "update", @payload
  end

  test "ignore clients with set timestamp", %{socket: socket} do
    time = Timex.now |> Timex.to_unix

    assert_push("update", @payload)

    push socket, "change_timestamp", time - 1_000_000

    assert_broadcast "update", %{rates: []}

    broadcast!()

    refute_broadcast "update", @payload

  end
end
