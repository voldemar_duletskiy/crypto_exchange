CryptoExchange.Repo.start_link([])
CryptoExchangeWeb.Endpoint.start_link([])
ExUnit.start()

{:ok, _} = Application.ensure_all_started(:ex_machina)
Application.ensure_all_started(:tzdata)