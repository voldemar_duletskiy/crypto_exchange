defmodule CryptoExchange.Context.RateTest do
  use CryptoExchange.ConnCase, async: true
  import CryptoExchange.Factory

  alias CryptoExchange.{Repo, Rate}

  setup do
    Repo.delete_all(Rate)
    :ok
  end

  describe "find_by/1" do
    test "find existing rate" do
      rate = insert(:rate)

      assert Rate.find_by("BTC") |> Repo.one() == rate
    end

    test "empy database" do
      Rate |> Repo.delete_all
      refute Rate.find_by("BTC") |> Repo.one()
    end
  end

  describe "find_by/2" do
    test "find by name and date" do
      currency = "ETH"

      _rate = insert(:rate, name: currency)

      date = Timex.shift(NaiveDateTime.utc_now(), days: -2)
      yesterday = Timex.shift(NaiveDateTime.utc_now(), days: -1)

      rate = insert(:rate, name: currency, rate: 1, inserted_at: date)

      result =
        Rate.find_by(currency, yesterday)
        |> Repo.all()
        |> Enum.map(& &1.id)

      assert result == [rate.id]
    end
  end
end
