defmodule CryptoExchange.Context.RatesTest do
  use CryptoExchange.ConnCase
  alias CryptoExchange.{Rate, Repo}
  alias CryptoExchange.Context.Rates

  @currencies Application.get_env(:crypto_exchange, :currencies)

  describe "fetch_rates/0" do
    test "increases number of rates in database" do
      assert length(Rates.fetch_rates()) == length(@currencies)
    end
  end

  describe "calculate/2" do
    setup do
      Rate |> Repo.delete_all
      :ok
    end

    test "calculate by currancy name and amount" do
      _rate = Rates.create(%{name: "ETH", rate: 1.0})

      assert Rates.calculate("ETH", 40) == 40.0
    end

    test "no rates" do
      assert Rates.calculate("ETH", 123) == {:error, "no rates for such currency"}
    end
  end

  @date Timex.now() |> Timex.shift(days: -3)
  @unix @date |> Timex.to_unix
  describe "calculate/3" do
    test "with date" do
      Rates.create(%{name: "ETH", rate: 1.0})
      _rate = Rates.create(%{name: "ETH", rate: 1.0, inserted_at: @date})

      assert Rates.calculate("ETH", 123, @date) == 123.0
      assert Rates.calculate("ETH", 123, @unix + 1) == 123.0
    end
  end

  describe "list/0" do
    test "list existing currencies" do
      assert Rates.list()
    end

    test "list existing currencies with date" do
      assert Rates.list(@unix)
    end
  end
end
