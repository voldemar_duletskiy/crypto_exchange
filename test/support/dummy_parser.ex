defmodule CryptoExchange.DummyParser do
  def parse() do
    %{
      "ETH" => 0.001344,
      "BTC" => 0.0001019,
      "BCH" => 0.0009252
    }
  end
end
