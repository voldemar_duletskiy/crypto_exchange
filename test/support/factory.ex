defmodule CryptoExchange.Factory do
  use ExMachina.Ecto, repo: CryptoExchange.Repo

  def rate_factory do
    %CryptoExchange.Rate{
      name: "BTC",
      rate: 0.0001019
    }
  end
end
