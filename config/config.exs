# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :crypto_exchange,
  ecto_repos: [CryptoExchange.Repo],
  parser: CryptoExchange.Parser

# Configures the endpoint
config :crypto_exchange, CryptoExchangeWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "Lh3x/i+ySBx680qHxkqOtLcH5OvRqk1wCuA3VglJ+qwB6Nk40J61FiBRKQgAXwsp",
  render_errors: [view: CryptoExchangeWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: CryptoExchange.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

config :crypto_exchange, :currencies, ["BTC", "BCH", "ETH"]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"

if File.exists?("config/#{Mix.env()}.local.exs") do
  import_config "#{Mix.env()}.local.exs"
end
